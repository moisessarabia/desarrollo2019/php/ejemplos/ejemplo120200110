<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>8</title>
    </head>
    <body>
        <form action="8.php" method="get">
            <label for="inombre">nombre del alumno</label>
            <input type="text" id="inombre" name="nombre"><label for="inumero1">numero1</label>
            <input type="number" name="numero[]" id="inumero1">
            <label for="inumero2">numero2</label>
             <input type="number" name="numero[]" id="inumero2">
             <div>
                 <label for="ipotes">Potes</label>
                 <input id="ipotes" type="checkbox" name="poblacion[]" value="potes" />
                 <label for="isantander">Santander</label>
                 <input id="isantander" type="checkbox" name="poblacion[]" value="santander" />
             </div>
             <div>
                 <label for="irojo">Rojo</label>
                 <input id="irojo" type="radio" name="colores" value="R">
                 <label for="iazul">Azul</label>
                 <input id="iazul" type="radio" name="colores" value="A">
             </div>
             <div>
                 <label for="inombres">selecciona nombres</label>
                 <select name="nombres[]" id="inombres" multiple="true">
                     <option value="0">Roberto</option>
                     <option value="1">Silvia</option>
                     <option value="2">Laura</option>
                 </select>
             </div>
             <button>enviar</button>
        </form>
        <?php
        // put your code here
        ?>
    </body>
</html>
